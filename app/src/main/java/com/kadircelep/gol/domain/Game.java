package com.kadircelep.gol.domain;

public class Game {
    private Generation currentGeneration;

    public Game(Generation initialGeneration) {
        this.currentGeneration = initialGeneration;
    }

    public void run(int steps) {
        currentGeneration = evolve(steps, currentGeneration);
    }

    public Generation getCurrentGeneration(){
        return currentGeneration;
    }

    private Generation evolve(int steps, Generation currentGeneration) {
        if (isOver(steps))
            return currentGeneration;
        return evolve(steps - 1, currentGeneration.produceNext());
    }

    public boolean isOver(int steps) {
        return steps == 0 || currentGeneration.isExtinct();
    }

    @Override
    public String toString() {
        return currentGeneration.toString();
    }
}
