package com.kadircelep.gol.domain;

public class ConwaysRules implements Rules {

    /* Each cell with one or no neighbors dies, as if by solitude.
    Each cell with four or more neighbors dies, as if by overpopulation.
    Each cell with two or three neighbors survives. */
    public boolean shouldStayAlive(int numberOfNeighbors) {
        return numberOfNeighbors == 2 || numberOfNeighbors == 3;

    }

    // Each cell with three neighbors becomes populated.
    public boolean shouldBeBorn(int numberOfNeighbors) {
        return numberOfNeighbors == 3;
    }
}
