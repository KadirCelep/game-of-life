package com.kadircelep.gol.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class Cell implements Parcelable {

    protected int i;
    protected int j;

    public Cell(int i, int j) {
        this.i = i;
        this.j = j;
    }

    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (getClass() != obj.getClass())
            return false;

        Cell other = (Cell) obj;
        if (i != other.i)
            return false;

        if (j != other.j)
            return false;

        return true;
    }

    public int getI() {
        return i;
    }

    public int getJ() {
        return j;
    }


    public Cells neighbors() {
        Cells cells = new Cells();
        cells.add(new Cell(i - 1, j - 1));
        cells.add(new Cell(i - 1, j));
        cells.add(new Cell(i - 1, j + 1));
        cells.add(new Cell(i, j - 1));
        cells.add(new Cell(i, j + 1));
        cells.add(new Cell(i + 1, j - 1));
        cells.add(new Cell(i + 1, j));
        cells.add(new Cell(i + 1, j + 1));
        return cells;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.i);
        dest.writeInt(this.j);
    }

    protected Cell(Parcel in) {
        this.i = in.readInt();
        this.j = in.readInt();
    }

    public static final Parcelable.Creator<Cell> CREATOR = new Parcelable.Creator<Cell>() {
        @Override
        public Cell createFromParcel(Parcel source) {
            return new Cell(source);
        }

        @Override
        public Cell[] newArray(int size) {
            return new Cell[size];
        }
    };

    @Override
    public String toString() {
        return "(" + i + "," + j + ")";
    }
}
