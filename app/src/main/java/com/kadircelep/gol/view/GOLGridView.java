package com.kadircelep.gol.view;

import android.content.Context;
import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.View;

import com.kadircelep.gol.domain.Cells;

public class GOLGridView extends View {
    public GOLGridView(Context context) {
        super(context);
        this.setFocusableInTouchMode(true);
        //TODO: Initialize gridsize
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    public void setCells(Cells cells) {
        //TODO: Set cells
        invalidate();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return true;
    }
}
