package com.kadircelep.gol.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.kadircelep.gol.R;
import com.kadircelep.gol.domain.Cell;

import java.util.ArrayList;
import java.util.List;

/**
 * Draws a square grid on canvas.
 * Size of the grid is calculated based on the attribute grid_edge_length.
 * Default edge length is 20 (results in a 20x20 grid)
 * <p/>
 * You can set or get the data represented as a List of Cell objects to be painted
 * <p/>
 * Kadir Celep, 2016
 */

public class GameGridView extends View {

    private static final String TAG = GameGridView.class.getSimpleName();

    private static final int GRID_EDGE_DEFAULT_LENGTH = 20;

    private float edgeLengthInPixels;

    public int gridEdgeLength;

    private Paint paintStroke;
    private Paint paintFill;

    // Had to use the implementation to be able to save it to Bundle.
    private ArrayList<Cell> cells;

    public GameGridView(Context context, AttributeSet attrs) {

        this(context, attrs, 0);

    }

    public GameGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        init(attrs);


        setPaints();

    }

    private void init(AttributeSet attrs) {
        if (attrs != null && !isInEditMode()) {

            final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.GameGridView);
            try {
                gridEdgeLength = a.getInt(R.styleable.GameGridView_grid_edge_length,
                        GRID_EDGE_DEFAULT_LENGTH);
            } finally {
                a.recycle();
            }

            cells = new ArrayList<>(gridEdgeLength * gridEdgeLength);
        }
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        float widthPx = getWidth() / gridEdgeLength;
        float heightPx = getHeight() / gridEdgeLength;

        edgeLengthInPixels = Math.min(widthPx, heightPx);

    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("superState", super.onSaveInstanceState());
        bundle.putParcelableArrayList("cells", this.cells);
        return bundle;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        if (state instanceof Bundle) {
            Bundle bundle = (Bundle) state;
            this.cells = bundle.getParcelableArrayList("cells");
            state = bundle.getParcelable("superState");
            invalidate();
        }
        super.onRestoreInstanceState(state);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawGrid(canvas);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (enabled) {
            paintFill.setAlpha(255);
        } else {
            paintFill.setAlpha(128);
        }
        invalidate();
    }

    private void drawGrid(Canvas canvas) {
        Rect globalVisibleRect = new Rect();
        getGlobalVisibleRect(globalVisibleRect);

        for (Cell cell : cells) {
            float left = cell.getI() * edgeLengthInPixels;
            float top = cell.getJ() * edgeLengthInPixels;
            float right = (cell.getI() + 1) * edgeLengthInPixels;
            float bottom = (cell.getJ() + 1) * edgeLengthInPixels;

            canvas.drawRect(left, top,
                    right, bottom, paintFill);
        }

        for (int i = 0; i < gridEdgeLength; i++) {
            for (int j = 0; j < gridEdgeLength; j++) {

                float left = j * edgeLengthInPixels;
                float top = i * edgeLengthInPixels;
                float right = (j + 1) * edgeLengthInPixels;
                float bottom = (i + 1) * edgeLengthInPixels;

                canvas.drawRect(left, top,
                        right, bottom, paintStroke);
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!isEnabled()) {
            return true;
        }
        float touchX = event.getX();
        float touchY = event.getY();
        int viewX = (int) (touchX);
        int viewY = (int) (touchY);

        int rowIndex = getRowIndex(viewY);
        int columnIndex = getColumnIndex(viewX);

        if (rowIndex < gridEdgeLength && columnIndex < gridEdgeLength) {
            Cell cell = new Cell(columnIndex, rowIndex);
            if (cells.contains(cell)) {
                cells.remove(cell);
            } else {
                cells.add(cell);
            }
            invalidate();

        }
        return super.onTouchEvent(event);
    }

    public void clear() {
        cells = new ArrayList<>();
        invalidate();
    }

    public ArrayList<Cell> getCells() {
        return cells;
    }

    public void setCells(List<Cell> cells) {
        this.cells.clear();
        for (Cell cell : cells) {
            if (cell.getI() < gridEdgeLength && cell.getJ() < gridEdgeLength) {
                this.cells.add(cell);
            }
        }
        invalidate();
    }

    private int getRowIndex(int viewY) {
        return (int) (viewY / edgeLengthInPixels);
    }

    private int getColumnIndex(int viewX) {
        return (int) (viewX / edgeLengthInPixels);
    }

    private void setPaints() {
        paintStroke = new Paint();
        paintStroke.setColor(getResources().getColor(R.color.colorPrimary));
        paintStroke.setStyle(Paint.Style.STROKE);
        paintStroke.setTextSize(18);

        paintFill = new Paint();
        paintFill.setStyle(Paint.Style.FILL);
        paintFill.setColor(getResources().getColor(R.color.colorPrimary));

    }

    public void setGridEdgeLength(int edgeLength) {
        ArrayList<Cell> currentCells = new ArrayList<>(cells.size());
        currentCells.addAll(cells);
        gridEdgeLength = edgeLength;
        // This will filter out overflows
        requestLayout();
        setCells(currentCells);
    }

    public int getGridEdgeLength() {
        return gridEdgeLength;
    }
}