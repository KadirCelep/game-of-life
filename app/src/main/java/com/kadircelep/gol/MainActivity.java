package com.kadircelep.gol;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.kadircelep.gol.domain.Cell;
import com.kadircelep.gol.domain.ConwaysRules;
import com.kadircelep.gol.domain.Game;
import com.kadircelep.gol.domain.Generation;
import com.kadircelep.gol.domain.LivingCells;
import com.kadircelep.gol.view.GameGridView;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    private GameGridView gameBoard;
    private Button startStop;
    private Button clear;
    private Button zoomIn;
    private Button zoomOut;
    private Game currentGame;
    private Subscription gameSubscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gameBoard = (GameGridView) findViewById(R.id.gameBoard);

        startStop = (Button) findViewById(R.id.startStop);
        startStop.setOnClickListener(onButtonStartStopClickedListener);

        clear = (Button) findViewById(R.id.clear);
        clear.setOnClickListener(onButtonClearClickedListener);

        zoomIn = (Button) findViewById(R.id.zoomIn);
        zoomIn.setOnClickListener(onZoomInClickedListener);

        zoomOut = (Button) findViewById(R.id.zoomOut);
        zoomOut.setOnClickListener(onZoomOutClickedListener);
    }

    private View.OnClickListener onButtonClearClickedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            gameBoard.clear();
        }
    };

    private View.OnClickListener onButtonStartStopClickedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // TODO: Create an observable based on game & button clicks and avoid code duplication.
            if (gameBoard.isEnabled()) {
                gameBoard.setEnabled(false);
                clear.setEnabled(false);
                startStop.setText(R.string.stop);
                startGame();
            } else {
                gameBoard.setEnabled(true);
                clear.setEnabled(true);
                startStop.setText(R.string.start);
                stopGame();
            }
        }
    };

    View.OnClickListener onZoomInClickedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            gameBoard.setGridEdgeLength(gameBoard.getGridEdgeLength() + 1);
            findViewById(R.id.zoomOut).setEnabled(true);
            if (gameBoard.getGridEdgeLength() == 30) {
                findViewById(R.id.zoomIn).setEnabled(false);
            } else {
                findViewById(R.id.zoomIn).setEnabled(true);
            }
        }
    };

    View.OnClickListener onZoomOutClickedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            gameBoard.setGridEdgeLength(gameBoard.getGridEdgeLength() - 1);
            findViewById(R.id.zoomIn).setEnabled(true);
            if (gameBoard.getGridEdgeLength() == 10) {
                findViewById(R.id.zoomOut).setEnabled(false);
            } else {
                findViewById(R.id.zoomOut).setEnabled(true);
            }
        }
    };

    //TODO: Moce game logic to a presenter and inject it.
    private void startGame() {
        gameBoard.setEnabled(false);
        clear.setEnabled(false);
        startStop.setText(R.string.stop);

        final ArrayList<Cell> currentCells = gameBoard.getCells();

        LivingCells livingCells = new LivingCells();
        for (Cell cell : currentCells) {
            livingCells.add(cell);
        }

        currentGame = new Game(new Generation(livingCells, new ConwaysRules()));

        gameSubscription = gameObservable
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Generation>() {
                    @Override
                    public void call(Generation generation) {
                        gameBoard.setCells(generation.getLivingCells().getCells());
                    }
                });

    }

    private void stopGame() {
        gameBoard.setEnabled(true);
        clear.setEnabled(true);
        startStop.setText(R.string.start);
        if (gameSubscription != null) {
            gameSubscription.unsubscribe();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        stopGame();
    }

    Observable<Generation> gameObservable = Observable.interval(500, TimeUnit.MILLISECONDS).map(new Func1<Long, Generation>() {
        @Override
        public Generation call(Long aLong) {
            if (!currentGame.isOver(1)) {
                currentGame.run(1);
            }
            return currentGame.getCurrentGeneration();
        }
    });
}
